#ifndef _UTIL_HPP
#define _UTIL_HPP
#include <string>
#include <string_view>
#include <vector>
#include <tuple>
#include <sstream>
#include <iomanip>
#include <cstdint>


namespace dpp {
class user;
}

namespace Util {
uint32_t get_color_of_user(const dpp::user& user);
std::vector<std::string_view> split_str(std::string_view s, char delimiter, size_t times);
std::string b64encode(const void* data, const size_t &len);
std::string b64decode(const void* data, const size_t &len);
void point_to_comma(std::string&);
unsigned str_to_color(std::string);


template <typename T>
std::string to_string_with_precision(T a_value, int n = 6) {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return out.str();
}

template<typename intT>
std::string int_as_hex(intT i) {
    std::ostringstream sstream;
    sstream << std::hex << i;
    return std::move(sstream).str();
}
inline
char hex_as_byte(char ch) {
    if (ch >= '0' && ch <= '9') return ch - '0';
    if (ch >= 'A' && ch <= 'F') return ch - 'A' + 10;
    if (ch >= 'a' && ch <= 'f') return ch - 'a' + 10;
    return 0;
}
template<typename intT>
intT hex_as_int(std::string_view i) {
    intT fres = 0;
    for (auto c : i) {
        fres <<= 4;
        fres |= hex_as_byte(c);
    }
    return fres;
};

inline
std::string b64encode(std::string_view str) {
    return b64encode(str.data(), str.size());
}
inline
std::string b64decode(std::string_view str64) {
    return b64decode(str64.data(), str64.size());
}
}
#endif // _UTIL_HPP
