# SomeBot

The SomeBot is a simple Discord Bot with the D++ Discord Library!

## Features

The bot is completely modular and allows the creation of different configurations for different bot tokens. D++ also allows auto-sharding and automatically starts the right number of shards.

### Modules

 * Captcha: Gives people a specific role when they answer a captcha
 * Embedmaker: Creates user-defined embeds
 * Fun: A couple of fun commands
 * Globalchat: Connects text channels on different servers with each other
 * Info: Displays information about the bot, server and user
 * Lastseen: Tracks user activity and shows when a user was last active
 * Levels: Assigns levels with user-defined speed and assigns roles
 * Management: Allows the bot to be managed from a central Discord server (bans, global chat moderation, etc.)
 * Ticket: Allows the creation and management of support tickets and saves logs

## Configuration

Here is an example configuration:

    [
        {
            "id": "tuxi",
            "management_guild_id": "1073679409623617556",
            "token": "ODAyODYzNTU0MjY4NjI2OTY1<redacted>r7zrQOCRI",
            "owner_id": "609486822715818000",
            "private": false,
            "modules_mode": "blacklist",
            "modules": ["Globalchat"]
        },
        {
            "id": "exxo",
            "token": "MTA0ODk3NjA3MDA4NDQxMTM5Mg<redacted>sCvig8wEA",
            "owner_id": "<redacted>",
            "private": true,
            "modules_mode": "whitelist",
            "modules": ["Captcha", "Levels", "Info", "Globalchat", "Lastseen", "Ticket", "Embedmaker"]
        }
    ]

And here is an explanation of the different options:

    {
        "id": "<name of the instance, any>",
        "token": "<bot token>",
        "owner_id": "<ID of the owner, optional>",
        "management_guild_id": "<ID of the management server, for the "Management" module>",
        "private": <whether the bot is private, i.e. will only be added by the user on servers>,
        "modules_mode": "<Whether the following module list is a blacklist or a whitelist (whitelist/blacklist), optional (default: whitelist)>",
        "modules": "<list of modules, see modules_mode>",
        "command_alias_mode", "<Which variants of the command names are used (all/first/last), optional (default: all)>"
    }

The configuration must be saved as `config.json` in the bot's working directory.

## Building

The bot is built with CMake and requires some submodules:

    git submodule update --init --depth 1 --recursive
    mkdir build
    cd build
    cmake ..
    make -j$(nproc)

The executable is created under `./SomeBot`.

## Usage

The executable takes no arguments, but if there are problems with the slash commands (after deactivating modules), they can be reset with `--reregister-commands`.
