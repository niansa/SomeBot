#include "../bot.hpp"

#include <thread>



class Fun {
    Bot *bot;

public:
    Fun(Bot *_bot) : bot(_bot) {
        bot->add_chatcommand(Bot::ChatCommand({"ping"}, "Ping pong!"), [&](const dpp::slashcommand_t& event) {
            event.reply(dpp::message("Pong!").set_flags(dpp::message_flags::m_ephemeral));
        });
    }
};
BOT_ADD_MODULE(Fun);
