#include "../bot.hpp"

#include <unordered_map>



class Join2create {
    Bot *bot;

    void db_add_guild(const dpp::snowflake& guild_id) {
        bot->db << "INSERT OR IGNORE INTO join2create_guild_settings (id) VALUES (?);"
                << std::to_string(guild_id);
    }

public:
    Join2create(Bot *_bot) : bot(_bot) {
        bot->db << "CREATE TABLE IF NOT EXISTS join2create_guild_settings ("
                   "    id TEXT PRIMARY KEY NOT NULL,"
                   "    role TEXT,"
                   "    category TEXT,"
                   "    UNIQUE(id)"
                   ");";
        bot->db << "CREATE TABLE IF NOT EXISTS join2create_guild_roles ("
                   "    guild_id TEXT NOT NULL,"
                   "    user_id TEXT NOT NULL,"
                   "    role_id TEXT NOT NULL,"
                   "    UNIQUE(guild_id, user_id)"
                   ");";

        bot->add_chatcommand(Bot::ChatCommand({"join2create_settings", "join2create_einstellungen"}, "Stelle die Einstellungen fuer Join2Create ein", dpp::slashcommand().add_option(dpp::command_option(dpp::command_option_type::co_role, "role", "Rolle", false)).add_option(dpp::command_option(dpp::command_option_type::co_channel, "category", "Kategorie", false))), [&](const dpp::slashcommand_t& event) {
            auto role = event.get_parameter("role");
            auto category = event.get_parameter("category");
            // Add guild to database
            db_add_guild(event.command.guild_id);
            // Apply role change
            if (role.index() != 0) {
                bot->db << "UPDATE join2create_guild_settings "
                           "SET role = ? "
                           "WHERE id = ?;"
                        << std::to_string(std::get<dpp::snowflake>(role)) << std::to_string(event.command.guild_id);
            }
            // Apply category change
            if (category.index() != 0) {
                bot->cluster.channel_get(std::get<dpp::snowflake>(category), [this, event](const dpp::confirmation_callback_t& ccb) {
                    if (!ccb.is_error()) {
                        auto channel = ccb.get<dpp::channel>();
                        // Ensure channel is part of THIS guild
                        if (channel.guild_id != event.command.guild_id) {
                            return;
                        }
                        // Use channel or parent channel as category
                        dpp::snowflake target;
                        if (channel.is_category()) {
                            target = channel.id;
                        } else if (channel.parent_id) {
                            target = channel.parent_id;
                        } else {
                            return;
                        }
                        // Update database
                        bot->db << "UPDATE join2create_guild_settings "
                                   "SET category = ? "
                                   "WHERE id = ?;"
                                << std::to_string(target) << std::to_string(event.command.guild_id);
                    }
                });
            }
            // Send reply
            event.reply(dpp::message("Okay.").set_flags(dpp::message_flags::m_ephemeral));
        });
    }
};
BOT_ADD_MODULE(Join2create);
