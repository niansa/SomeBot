#include <dpp/dpp.h>

namespace GlobalchatExternal {
struct MessageInfo {
    static constexpr std::string_view message_code_idenfitication = "https://internal.keineahnungbot.net/discordInspectionData/";

    dpp::snowflake author, message, channel, guild;

    struct DecodeError : public std::logic_error {
        DecodeError() : std::logic_error("Error decoding the global chat message code") {}
    };

    std::string encode() const;
    static MessageInfo decode(std::string_view str);
    static MessageInfo decode_message(const dpp::message& msg);
};
}
