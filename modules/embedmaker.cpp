#include "../bot.hpp"
#include "../util.hpp"



class Embedmaker {
    Bot *bot;

    static inline const std::string& strOrEmpty(const dpp::command_value& v) {
        if (v.index() == 0) {
            static std::string empty;
            return empty;
        } else {
            return std::get<std::string>(v);
        }
    }

public:
    Embedmaker(Bot *_bot) : bot(_bot) {
        bot->cluster.intents |= dpp::intents::i_guild_messages | dpp::intents::i_message_content;

        bot->add_chatcommand(Bot::ChatCommand({"make_embed"}, "Create an embed", dpp::slashcommand()
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "title", "Title", true))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "description", "Description", true))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "image", "Picture", false))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "video", "Video", false))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "thumbnail", "Tiny picture", false))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "color", "Color", false))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "footer_text", "Footer text", false))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "footer_image", "Tiny footer picture", false))
                                              .add_option(dpp::command_option(dpp::command_option_type::co_string, "content", "Text", false))), [&](const dpp::slashcommand_t& event) {
            // Check that user has the correct permissions
            if (!event.command.get_guild().base_permissions(event.command.member).has(dpp::permissions::p_manage_messages)) {
                event.reply(dpp::message("You need message management permissions to use this command.").set_flags(dpp::message_flags::m_ephemeral));
                return;
            }
            // Get color
            auto colorStr = strOrEmpty(event.get_parameter("color"));
            unsigned color = Util::str_to_color(colorStr.empty()?"black":colorStr);
            // Generate embed
            dpp::embed embed;
            embed.set_title(strOrEmpty(event.get_parameter("title")))
                 .set_description(strOrEmpty(event.get_parameter("description")))
                 .set_image(strOrEmpty(event.get_parameter("image")))
                 .set_video(strOrEmpty(event.get_parameter("video")))
                 .set_thumbnail(strOrEmpty(event.get_parameter("thumbnail")))
                 .set_color(color!=-1U?color:0xffffff)
                 .set_footer(strOrEmpty(event.get_parameter("footer_text")), strOrEmpty(event.get_parameter("footer_image")));
            // Send embed
            bot->cluster.message_create(dpp::message().set_content(strOrEmpty(event.get_parameter("content"))).set_channel_id(event.command.channel_id).add_embed(embed));
            // Report success
            event.reply(dpp::message(std::string("Your embed has been created!")
                                     +(color==-1U?"\n**Attention:** The specified color was not recognized! Try again with a color code.":"")).set_flags(dpp::message_flags::m_ephemeral));
        });
    }
};
BOT_ADD_MODULE(Embedmaker);
