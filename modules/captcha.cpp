#include "../bot.hpp"
#include "../util.hpp"

#include <thread>
#include <chrono>
#include <mutex>
#include <ctime>

#include <dpp/nlohmann/json.hpp>



// https://captcha-api.akshit.me/v2/test

class Captcha {
    Bot *bot;

    std::unordered_map<std::string, std::string> uuids;
    std::shared_mutex uuid_mutex;

    void db_add_guild(dpp::snowflake guild_id) {
        bot->db << "INSERT OR IGNORE INTO captcha_guild_settings (id) VALUES (?);"
                << std::to_string(guild_id);
    }

    void set_guild_settings(dpp::snowflake guild_id, dpp::snowflake captcha_channel_id, dpp::snowflake verified_role_id) {
        // Unset global chat
        bot->db << "UPDATE captcha_guild_settings "
                   "SET captcha_channel = ?, "
                   "    verified_role = ? "
                   "WHERE id = ?;"
                << std::to_string(captcha_channel_id)
                << std::to_string(verified_role_id)
                << std::to_string(guild_id);
        return;
    }
    void reset_guild_settings(dpp::snowflake guild_id) {
        // Unset global chat
        bot->db << "UPDATE captcha_guild_settings "
                   "SET captcha_channel = NULL, "
                   "    verified_role = NULL "
                   "WHERE id = ?;"
                << std::to_string(guild_id);
        return;
    }
    std::pair<dpp::snowflake, dpp::snowflake> get_guild_settings(dpp::snowflake guild_id) {
        // Read from database
        std::string channel_id_str, role_id_str;
        try {
            bot->db << "SELECT captcha_channel, verified_role FROM captcha_guild_settings "
                       "WHERE id = ?;"
                    << std::to_string(guild_id)
                    >> std::tie(channel_id_str, role_id_str);
        } catch (...) {
            return {0, 0};
        }
        // Get as snowflakes
        dpp::snowflake channel_id = 0, role_id = 0;
        if (!channel_id_str.empty()) {
            channel_id = channel_id_str;
        }
        if (!role_id_str.empty()) {
            role_id = role_id_str;
        }
        // Return as pair
        return {channel_id, role_id};
    }

    void send_embed(dpp::snowflake channel_id) {
        dpp::message msg;
        msg.set_channel_id(channel_id)
           .add_embed(dpp::embed()
                      .set_title("Captcha")
                      .set_description("Please verify yourself!")
                      .set_footer("You have 1 minute and 30 seconds to solve the captcha.", "https://whatemoji.org/wp-content/uploads/2020/07/Alarm-Clock-Emoji.png"))
           .add_component(dpp::component()
                          .add_component(dpp::component()
                                         .set_label("I'm not a robot")
                                         .set_type(dpp::component_type::cot_button)
                                         .set_emoji("✅")
                                         .set_style(dpp::component_style::cos_primary)
                                         .set_id("captcha_verify")));

        bot->cluster.message_create(msg);
    }

    auto autodelete_message(const dpp::message& msg) {
        std::thread([this, msg]() {
            std::this_thread::sleep_for(std::chrono::seconds(7));
            bot->cluster.message_delete(msg.id, msg.channel_id);
        }).detach();
    }
    auto autodelete_message_cb() {
        return [this](const dpp::confirmation_callback_t& ccb) {
            if (ccb.is_error()) {
                dpp::utility::log_error();
                return;
            }
            autodelete_message(ccb.get<dpp::message>());
        };
    }

public:
    Captcha(Bot *_bot) : bot(_bot) {
        bot->cluster.intents |= dpp::intents::i_guild_members;

        bot->db << "CREATE TABLE IF NOT EXISTS captcha_guild_settings ("
                   "    id TEXT PRIMARY KEY NOT NULL,"
                   "    captcha_channel TEXT,"
                   "    verified_role TEXT,"
                   "    UNIQUE(id)"
                   ");";

        bot->add_chatcommand(Bot::ChatCommand({"captcha"}, "Configure a captcha channel", dpp::slashcommand().add_option(dpp::command_option(dpp::command_option_type::co_channel, "channel", "Channel in which the challenge is shown", true)).add_option(dpp::command_option(dpp::command_option_type::co_role, "role", "Role given after solving challenge", true))), [&](const dpp::slashcommand_t& event) {
            // Check that user has the correct permissions
            if (!event.command.get_guild().base_permissions(event.command.member).has(dpp::permissions::p_manage_channels | dpp::permissions::p_manage_roles)) {
                event.reply(dpp::message("You need channel and role management permissions to use this command.").set_flags(dpp::message_flags::m_ephemeral));
                return;
            }
            // Update database
            auto channel_id = std::get<dpp::snowflake>(event.get_parameter("channel"));
            auto role_id = std::get<dpp::snowflake>(event.get_parameter("role"));
            db_add_guild(event.command.guild_id);
            set_guild_settings(event.command.guild_id, channel_id, role_id);
            send_embed(channel_id);
            // Send reply
            event.reply(dpp::message("Done.").set_flags(dpp::message_flags::m_ephemeral));
        });
        bot->add_chatcommand(Bot::ChatCommand({"no_captcha", "kein_captcha", "captcha_stop"}, "Deaktiviere das Captcha"), [&](const dpp::slashcommand_t& event) {
            // Check that user has the correct permissions
            if (!event.command.get_guild().base_permissions(event.command.member).has(dpp::permissions::p_manage_channels | dpp::permissions::p_manage_roles)) {
                event.reply(dpp::message("You need channel and role management permissions to use this command.").set_flags(dpp::message_flags::m_ephemeral));
                return;
            }
            // Update database
            reset_guild_settings(event.command.guild_id);
            // Send reply
            event.reply(dpp::message("Done.").set_flags(dpp::message_flags::m_ephemeral));
        });

        bot->cluster.on_guild_member_add([this](const dpp::guild_member_add_t& event) { // Ghost ping
            auto [channel_id, role_id] = get_guild_settings(event.adding_guild->id);
            // Check that channel is set
            if (channel_id == dpp::snowflake(0)) {
                return;
            }
            // Ghost ping
            bot->cluster.message_create(dpp::message().set_content(event.added.get_mention()+", notice me!"), [this, guild_id = event.adding_guild->id](const dpp::confirmation_callback_t& ccb) {
                if (ccb.is_error()) {
                    reset_guild_settings(guild_id);
                    return;
                }
                // Delete message
                auto msg = ccb.get<dpp::message>();
                bot->cluster.message_delete(msg.id, msg.channel_id);
            });
        });

        bot->cluster.on_button_click([this](const dpp::button_click_t& event) {
            if (event.custom_id == "captcha_verify") {
                // Make sure user isn't a known bot
                if (event.command.usr.is_bot()) {
                    return;
                }
                // Receive captcha from server
                bot->cluster.request("https://captcha-api.akshit.me/v2/generate", dpp::http_method::m_get, [this, event](const dpp::http_request_completion_t& ccb) {
                    // Check for error
                    if (ccb.status != 200) {
                        event.reply("Error loading the captcha, try again later or contact the management.");
                        return;
                    }
                    // Deserialize JSON
                    auto data = nlohmann::json::parse(ccb.body);
                    // Get UUID list key
                    auto uuid_key = std::to_string(event.command.guild_id)+std::to_string(event.command.usr.id);
                    {
                        // Lock UUID list
                        std::scoped_lock L(uuid_mutex);
                        // Assign UUID to user
                        uuids[uuid_key] = data["uuid"];
                    }
                    // Convert image from base64
                    std::string data_url = data["captcha"];
                    auto jpeg_b64_data = Util::split_str(data_url, ',', 1)[1];
                    auto jpeg_binary_data = Util::b64decode(jpeg_b64_data);
                    // Create message
                    dpp::message msg;
                    msg.set_channel_id(event.command.channel_id)
                       .set_content("> Expires in <t:"+std::to_string(time(nullptr)+90)+":R>\nPlease enter the characters shown in this image:")
                       .add_file("captcha.jpeg", jpeg_binary_data)
                       .set_flags(dpp::message_flags::m_ephemeral);
                    // Upload captcha
                    event.reply(msg);
                    // Delete captcha after 30 seconcds
                    std::thread([this, event, uuid_key]() {
                        std::this_thread::sleep_for(std::chrono::seconds(90));
                        // Lock UUID list and find UUID in it
                        std::scoped_lock L(uuid_mutex);
                        auto res = uuids.find(uuid_key);
                        // Remove from UUID list and send timeout message if it was found
                        if (res != uuids.end()) {
                            uuids.erase(res);
                        }
                        // Delete message
                        event.edit_original_response(dpp::message().set_content("The time has expired."));
                    }).detach();
                });
            }
        });

        bot->cluster.on_message_create([this](const dpp::message_create_t& event) {
            // Lock UUID list
            std::scoped_lock L(uuid_mutex);
            // Get server captcha settings
            auto settings = get_guild_settings(event.msg.guild_id);
            // Make sure that this is the correct channel
            if (settings.first != event.msg.channel_id) {
                return;
            }
            // Delete message later
            autodelete_message(event.msg);
            // Make sure user isn't a known bot
            if (event.msg.author.is_bot()) {
                return;
            }
            // Find user is in UUID list
            auto uuid_key = std::to_string(event.msg.guild_id)+std::to_string(event.msg.author.id);
            auto res = uuids.find(uuid_key);
            if (res == uuids.end())  {
                // UUID isn't known
                event.reply("Please start a new captcha.", true, autodelete_message_cb());
                return;
            }
            // Lowercase captcha
            std::string captcha = event.msg.content;
            for (auto& c : captcha) {
                c = tolower(c);
            }
            // Request API to validate response
            auto json_body = "{\"uuid\": \""+res->second+"\", \"captcha\": \""+captcha+"\"}";
            bot->cluster.request("https://captcha-api.akshit.me/v2/verify", dpp::http_method::m_post, [this, event, res, uuid_key, settings, json_body](const dpp::http_request_completion_t& ccb) {
                std::scoped_lock L(uuid_mutex);
                if (ccb.status == 200) {
                    // Verification successful, assign role
                    bot->cluster.guild_member_add_role(event.msg.guild_id, event.msg.author.id, settings.second, [this, event](const dpp::confirmation_callback_t& ccb) {
                        if (ccb.is_error()) {
                            event.reply("Verification successful, but role could not be assigned.", true, autodelete_message_cb());
                        } else {
                            event.reply("Verification successful, role has been assigned.", true, autodelete_message_cb());
                        }
                    });
                    // Remove UUID from list
                    uuids.erase(res);
                } else if (ccb.status == 429) {
                    // Too many tries
                    event.reply("Too many attempts. Create a new captcha.", true, autodelete_message_cb());
                    uuids.erase(res);
                } else if (ccb.status == 404) {
                    // Not found (shouldn't ever be triggered)
                    event.reply("Captcha has expired. Create a new captcha.", true, autodelete_message_cb());
                } else if (ccb.status == 401) {
                    // Wrong response
                    event.reply("Wrong answer. Try again.", true, autodelete_message_cb());
                } else{
                    // Other error
                    event.reply("Invalid server response "+std::to_string(ccb.status)+". Create a new captcha.```json\n"+json_body+"``````json\n"+ccb.body+"\n```", true, autodelete_message_cb());
                    uuids.erase(res);
                }
            }, json_body, "application/json", {{"Content-Type", "application/json"}});
        });
    }
};
BOT_ADD_MODULE(Captcha);
