#ifndef _MODULES_INFO_HPP
#define _MODULES_INFO_HPP
#include <dpp/dpp.h>


namespace InfoExternal {
    dpp::embed generate_user_info_embed(const dpp::user& user);
}
#endif // _MODULES_INFO_HPP
