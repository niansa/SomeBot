#include "../bot.hpp"

#include <ctime>



class Lastseen {
    Bot *bot;

    void db_add_user(dpp::snowflake user_id) {
        bot->db << "INSERT OR IGNORE INTO lastseen (id, time, optout) VALUES (?, 0, FALSE);"
                << std::to_string(user_id);
    }

    time_t get_last_online_time(dpp::snowflake user_id) {
        time_t time = 0;
        bool optout = false;
        bot->db << "SELECT time, optout FROM lastseen "
                   "WHERE id = ?;"
                << std::to_string(user_id)
                >> [&](time_t _time, bool _optout) {
            time = _time;
            optout = _optout;
        };
        if (optout) {
            return 0;
        } else {
            return time;
        }
    }

    void update_last_online_time(dpp::snowflake user_id) {
        auto time = ::time(nullptr);
        bot->db << "UPDATE lastseen "
                   "SET time = ? "
                   "WHERE id = ?;"
                << time
                << std::to_string(user_id);
    }

    std::string last_seen_string(dpp::snowflake user_id, const std::string& username = "die Person") {
        // Get from database
        auto time = get_last_online_time(user_id);
        // Send reply
        if (time) {
            return "Last time I've seen "+username+" was <t:"+std::to_string(time)+":R>!";
        } else {
            return "I can't tell you when I last saw "+username+" online.";
        }
    }

public:
    Lastseen(Bot *_bot) : bot(_bot) {
        bot->cluster.intents |= dpp::intents::i_guild_presences;

        bot->db << "CREATE TABLE IF NOT EXISTS lastseen ("
                   "    id TEXT PRIMARY KEY NOT NULL,"
                   "    optout INTEGER,"
                   "    time INTEGER,"
                   "    UNIQUE(id)"
                   ");";

        bot->add_chatcommand(Bot::ChatCommand({"last-seen", "last-online"}, "See when a person was last online", dpp::slashcommand().add_option(dpp::command_option(dpp::command_option_type::co_user, "user", "Zielbenutzer", true))), [&](const dpp::slashcommand_t& event) {
            event.reply(dpp::message(last_seen_string(std::get<dpp::snowflake>(event.get_parameter("user")))).set_flags(dpp::message_flags::m_ephemeral));
        });

        bot->add_usercommand(Bot::UserCommand({"Last seen..."}, "See when a person was last online"), [&](const dpp::user_context_menu_t& event) {
            event.reply(dpp::message(last_seen_string(event.get_user().id, event.get_user().username)).set_flags(dpp::message_flags::m_ephemeral));
        });

        bot->cluster.on_presence_update([this](const dpp::presence_update_t& event) {
            db_add_user(event.rich_presence.user_id); //TODO: Avoid
            update_last_online_time(event.rich_presence.user_id);
        });
    }
};
BOT_ADD_MODULE(Lastseen);
