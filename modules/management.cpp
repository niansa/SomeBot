#include "../bot.hpp"

#include <thread>
#include <algorithm>



class Management {
    Bot *bot;

    std::vector<dpp::snowflake> guilds;

public:
    Management(Bot *_bot) : bot(_bot) {
        bot->cluster.intents |= dpp::intents::i_guilds;

        bot->add_chatcommand(Bot::ChatCommand({"guild_list", "server_list"}, "Shows lists of servers the bot is on"), [&](const dpp::slashcommand_t& event) {
            event.thinking();
            std::thread([this, event]() {
                // Get each server
                std::vector<dpp::guild> detailed_guilds;
                for (const auto guild_id : guilds) {
                    try {
                        detailed_guilds.push_back(bot->cluster.guild_get_sync(guild_id));
                    } catch (...) {}
                }
                // Compose embed
                dpp::embed embed;
                embed.set_title("Server list")
                     .set_description("I'm on the following servers:");
                for (const auto& guild : detailed_guilds) {
                    embed.add_field(guild.name, std::to_string(guild.id));
                }
                // Send final embed
                event.edit_original_response(dpp::message().add_embed(embed));
            }).detach();
        }, bot->config.management_guild_id);
        bot->add_chatcommand(Bot::ChatCommand({"guild_leave", "server_leave"}, "Remove the bot from a server", dpp::slashcommand().add_option(dpp::command_option(dpp::command_option_type::co_string, "guild_id", "Server ID", true))), [&](const dpp::slashcommand_t& event) {
            // Get guild ID
            dpp::snowflake guild_id;
            try {
                guild_id = std::get<std::string>(event.get_parameter("guild_id"));
            } catch (...) {
                event.reply(dpp::message("This is not a valid ID.").set_flags(dpp::message_flags::m_ephemeral));
            }
            // Make sure guild ID isn't management server
            if (guild_id == dpp::snowflake(bot->config.management_guild_id)) {
                event.reply(dpp::message("I will not leave the management server.").set_flags(dpp::message_flags::m_ephemeral));
                return;
            }
            // Try to leave server
            bot->cluster.current_user_leave_guild(guild_id, [event, guild_id](const dpp::confirmation_callback_t& ccb) {
                if (ccb.is_error()) {
                    event.reply("I couldn't leave the server `"+std::to_string(guild_id)+"` because: "+ccb.get_error().message);
                } else {
                    event.reply(dpp::message("I have successfully left the specified server.").set_flags(dpp::message_flags::m_ephemeral));
                }
            });
        }, bot->config.management_guild_id);

        bot->cluster.on_guild_create([&](const dpp::guild_create_t& guild) {
            guilds.push_back(guild.created->id);
        });
        bot->cluster.on_guild_delete([&](const dpp::guild_delete_t& guild) {
            std::remove(guilds.begin(), guilds.end(), guild.deleted->id);
        });
    }
};
BOT_ADD_MODULE(Management);
